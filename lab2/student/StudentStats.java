package lab2.student;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class StudentStats {
	
	public static final void main(String args[]) {

		List<Student> students = IntStream.range(1, 50)
										  .mapToObj(it->
										  			new Student(StudentUtil::generateName,
													StudentUtil::generateGrade,
													StudentUtil::generateDepartment,
													StudentUtil::generateGender))
										  .collect(Collectors.toList());
		
		//1. compute the number of students with passing grades.
		//2. compute the  average student grade.
		//3. partition the students to their belonging department.
		//4. compute the average grade by department.
}
